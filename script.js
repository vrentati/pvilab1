// script.js

function addStudent() {
    var newRow = '<tr>' +
        '<td class="align-middle">' +
        '<div class="checkbox-container">' +
        '<input class="custom-checkbox2" type="checkbox">' +
        '</div>' +
        '</td>' +
        '<td>KN-23</td>' +
        '<td>Адам Адамс</td>' +
        '<td>M</td>' +
        '<td>01.01.2000</td>' +
        '<td class="align-middle">' +
        '<div class="checkbox-container">' +
        '<input class="custom-checkbox" type="checkbox">' +
        '</div>' +
        '</td>' +
        '<td class="align-middle">' +
        '<div class="d-flex justify-content-center align-items-center actionIcons">' +
        '<button class="icon-btn btn btn-outline-secondary btn-sm" onclick="deleteRow(this)"><i class="bi bi-x"></i></button>' +
        '<button class="icon-btn btn btn-outline-secondary btn-sm"><i class="bi bi-pencil"></i></button>' +
        '</div>' +
        '</td>' +
        '</tr>';
    $('table tbody').append(newRow);
}

$(document).ready(function() {
    $('#addStudent').on('click', addStudent);
});

function deleteRow(button) {
    var row = button.closest('tr');
    var table = row.parentNode;
    // Delete the row
    table.removeChild(row);
}
